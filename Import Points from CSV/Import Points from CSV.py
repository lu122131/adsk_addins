import adsk.core, adsk.fusion, adsk.cam, traceback
import io

# This is only needed with Python.
handlers = []

# Global data points
data = []

# Selections
selectedSketch = None

# User interface
ui = None
app = None

previouslySelectedRefPlan = None

def run(context):
    global ui
    global app
    
    try:
        app = adsk.core.Application.get()
        ui = app.userInterface

        # Get the CommandDefinitions collection.
        cmdDefs = ui.commandDefinitions
        
        # Create a button command definition.
        importPointsFromCsv_btn = cmdDefs.addButtonDefinition('ImportPointsFromCsv', 
                                                   'Import Points from CSV', 
                                                   'Import Points From CSV file',
                                                   '/Resources/CmdIcons')
        
        # Connect to the command created event.
        onCmdCreatedHandler = cmdCreatedEventHandler()
        importPointsFromCsv_btn.commandCreated.add(onCmdCreatedHandler)
        handlers.append(onCmdCreatedHandler)
       
        # Get Tools Tab
        toolsTab = ui.allToolbarTabs.itemById('ToolsTab')

        # Add Custom Panel as last one in Tools Tab
        import_TbP = toolsTab.toolbarPanels.add('importtbp','IMPORT')

        # Add button as last command in the Toolbar Panel
        buttonControl = import_TbP.controls.addCommand(importPointsFromCsv_btn)

    except:
        if ui:
            ui.messageBox('Failed:\n{}'.format(traceback.format_exc()))

def stop(context):
    global ui
    try:
        # Clean up the UI.
        cmdDef = ui.commandDefinitions.itemById('ImportPointsFromCsv')

        if cmdDef:
            cmdDef.deleteMe()

        toolsTab = ui.allToolbarTabs.itemById('ToolsTab')
        import_TbP = toolsTab.toolbarPanels.itemById('importtbp')

        if import_TbP:
            buttonControl = import_TbP.controls.itemById('ImportPointsFromCsv')

            if buttonControl:
                buttonControl.deleteMe()
            
            import_TbP.deleteMe()

    except:
        if ui:
            ui.messageBox('Failed:\n{}'.format(traceback.format_exc()))	

# Event handler for the commandCreated event.
class cmdCreatedEventHandler(adsk.core.CommandCreatedEventHandler):
    def __init__(self):
        super().__init__()
    def notify(self, args):   

        global ui

        data.clear()
        selectedSketch = None     

        # --------------- Ask for data file to import ---------------------------------------     
        dlg = ui.createFileDialog()
        dlg.title = 'Open CSV File'
        dlg.filter = 'Comma Separated Values (*.csv);;All Files (*.*)'
        if dlg.showOpen() != adsk.core.DialogResults.DialogOK :
            return
               
        filename = dlg.filename
        firstLine = True
        pointsNum = 0

        with io.open(filename, 'r', encoding='utf-8-sig') as f:
            line = f.readline()
            while line:
                pntStrArr = line.split(',')
                if firstLine :
                    pointsNum = len(pntStrArr)                    
                    data.append([float(i) for i in pntStrArr])
                    firstLine = False
                elif len(pntStrArr) == pointsNum :
                    data.append([float(i) for i in pntStrArr])
                else :
                    ui.messageBox('File format invalid [e100]')
                    return
                line = f.readline()

        # --------------- Create command Inputs ---------------------------------------------
        
        # Get the command from args
        eventArgs = adsk.core.CommandCreatedEventArgs.cast(args)               
        cmd = eventArgs.command

        # Get the CommandInputs collection to create new command inputs.            
        inputs = cmd.commandInputs

        # Description Label
        lblTxt = '<b>Importing ' + str(len(data)) + ' points [' + str(len(data[0])) + '-dim] as: </b>'
        inputs.addTextBoxCommandInput('lblCmdInput', '', lblTxt, 1, True)

        # Create dropdown input with test list style.
        measUnitDropDown = inputs.addDropDownCommandInput('unitmeasDropdown', 'Unit fo Measure', adsk.core.DropDownStyles.TextListDropDownStyle)
        measUnitDropDownItems = measUnitDropDown.listItems
        measUnitDropDownItems.add('cm', True, '')
        measUnitDropDownItems.add('mm', False, '')

        # Create buttons to select the plane for the new sketch
        buttonRowInput = inputs.addButtonRowCommandInput('SelectPlaneBtnRow', 'Reference Plane', False)
        buttonRowInput.listItems.add('XY', True, '/Resources/Buttons/xy')
        buttonRowInput.listItems.add('YZ', False, '/Resources/Buttons/yz')
        buttonRowInput.listItems.add('XZ', False, '/Resources/Buttons/xz')

        # Select an existing sketch
        selectInput = inputs.addSelectionInput('SketchSelection', 'Existing Sketch', 'Select an existing sketch')
        selectInput.addSelectionFilter(adsk.core.SelectionCommandInput.Sketches)
        selectInput.setSelectionLimits(0, 1)

        # Radio Button to select import as spline, scattered points, line
        radioButtonGroup = inputs.addRadioButtonGroupCommandInput('radioButtonGroupSelection', '')
        radioButtonItems = radioButtonGroup.listItems
        radioButtonItems.add('Spline', True)
        radioButtonItems.add('Line', False)
        radioButtonItems.add('Scattered Points', False)

        # ------------- Connect to Handlers ----------------------------------------------
        # Select Handler registration
        onSelect = MySelectHandler()
        cmd.select.add(onSelect)
        handlers.append(onSelect)

        # De-Select Handler registration
        onUnSelect = MyUnSelectHandler()
        cmd.unselect.add(onUnSelect)            
        handlers.append(onUnSelect) 

        # Input changed Handler
        onInputChanged = onInputChange()
        cmd.inputChanged.add(onInputChanged)
        handlers.append(onInputChanged)
        
        # On Execute Handler
        onExecute = onExecuteHandler()
        cmd.execute.add(onExecute)
        handlers.append(onExecute)

        # # On Destroy Handler
        # onDestroy = onDestryHandler()
        # cmd.destroy.add(onDestroy)
        # handlers.append(onDestroy)

# Event handler for the execute event.
class onExecuteHandler(adsk.core.CommandEventHandler):
    def __init__(self):
        super().__init__()
    def notify(self, args):

        global ui
        global selectedSketch

        try:
            eventArgs = adsk.core.CommandEventArgs.cast(args)
            inputs = eventArgs.command.commandInputs

            # Create a points collection
            points = adsk.core.ObjectCollection.create()

            # Get the values from the command inputs
            buttonRowInput = adsk.core.ButtonRowCommandInput.cast(inputs.itemById('SelectPlaneBtnRow'))

            # Get the unit of measure of the imported points
            measUnitDropDown = adsk.core.DropDownCommandInput.cast(inputs.itemById('unitmeasDropdown'))            
            
            # If imported data are in mm convert it, because the API works with cm
            # according to https://help.autodesk.com/view/fusion360/ENU/?guid=GUID-A81B295F-984A-4039-B1CF-B449BEE893D7
            if measUnitDropDown.selectedItem.name == 'mm':
                for i in range(len(data)):
                    data[i] = [round((elmnt * 0.1), 15) for elmnt in data[i]]

            # Get the selected sketch or create a new one according
            # to the user plane selection
            if selectedSketch:
                # Use selected sketch
                sketch = selectedSketch
            else:                
                # Get the plan for a new sketch
                root = adsk.fusion.Design.cast(app.activeProduct).rootComponent

                if buttonRowInput.selectedItem.name == 'XY':
                    constructionPlane = root.xYConstructionPlane
                elif buttonRowInput.selectedItem.name == 'XZ':
                    constructionPlane = root.xZConstructionPlane             
                else:
                    constructionPlane = root.yZConstructionPlane             

                sketch = root.sketches.add(constructionPlane)

            # Create points from data (2d or 3d)
            # considering conversion from Model-Space to Sketch-Space
            if len(data[0]) == 2:
                for pt in data:                    
                    if buttonRowInput.selectedItem.name == 'XY':
                        points.add(sketch.modelToSketchSpace(adsk.core.Point3D.create(pt[0], pt[1], 0)))  
                    elif buttonRowInput.selectedItem.name == 'XZ':
                        points.add(sketch.modelToSketchSpace(adsk.core.Point3D.create(pt[0], 0, pt[1])))             
                    else:
                        points.add(sketch.modelToSketchSpace(adsk.core.Point3D.create(0, pt[0], pt[1])))                                
            elif len(data[0]) == 3:
                for pt in data:
                    points.add(sketch.modelToSketchSpace(adsk.core.Point3D.create(pt[0], pt[1], pt[2])))
            else:
                points.clear()
            
            # If there are points to plot do it
            if points.count:

                radioButtonGroup = adsk.core.RadioButtonGroupCommandInput.cast(inputs.itemById('radioButtonGroupSelection'))
                selectedItem = radioButtonGroup.selectedItem

                if selectedItem.name == 'Spline':
                    # Import points as Spline
                    sketch.sketchCurves.sketchFittedSplines.add(points)

                elif selectedItem.name == 'Line':
                    # Import points as Line
                    for i in range(len(points)-1):
                        pt1 = points[i]
                        pt2 = points[i+1]
                        sketch.sketchCurves.sketchLines.addByTwoPoints(pt1, pt2)
                    
                else:
                    # Import Points as Scattered Points                
                    for i in range(len(points)):
                        sketch.sketchPoints.add(points[i])           
                    
                    # Used the previous loop, because the following
                    # give Exception has occurred: IndexError
                    # The index (6) is out of range.
                    #for pp in points:
                    #    sketch.sketchPoints.add(pp)
                    # As Autodesk said on its forum:
                    # adsk.core.ObjectCollection.create()
                    # doesn't implement iterator yet

            else:
                ui.messageBox('No valid points')

            # Reset variables
            data.clear()
            points.clear()
            selectedSketch = None
            
        except:
            if ui:
                ui.messageBox('Failed:\n{}'.format(traceback.format_exc()))            

# Event fired in case of input changes to manage the panel button selected in case of sketch selection 
class onInputChange(adsk.core.InputChangedEventHandler):
    def __init__(self):
        super().__init__()
    def notify(self, args):

        global ui
        global previouslySelectedRefPlan

        try:
            # Get Inputs from args
            eventArgs = adsk.core.InputChangedEventArgs.cast(args)
            inputs = eventArgs.inputs

            # Get Changed Input
            cmdInput = eventArgs.input

            if cmdInput.id == 'SketchSelection':
                buttonRowInput = adsk.core.ButtonRowCommandInput.cast(inputs.itemById('SelectPlaneBtnRow'))

                if cmdInput.selectionCount == 1:
                    sketch = adsk.fusion.Sketch.cast(cmdInput.selection(0).entity)
                    
                    if sketch and buttonRowInput:                        
                        previouslySelectedRefPlan = buttonRowInput.selectedItem
                        buttonRowInput.isEnabled = False
                        
                        for btn in buttonRowInput.listItems:
                            if btn.name == sketch.referencePlane.name:
                                btn.isSelected = True
                            else:
                                btn.isSelected = False                            
                else:
                    if buttonRowInput:
                        for btn in buttonRowInput.listItems:
                            btn.isSelected = False

                        previouslySelectedRefPlan.isSelected = True
                        buttonRowInput.isEnabled = True
                        previouslySelectedRefPlan = None          
        except:
            if ui:
                ui.messageBox('Failed:\n{}'.format(traceback.format_exc()))

# Selected Handlers
class MySelectHandler(adsk.core.SelectionEventHandler):
    def __init__(self):
        super().__init__()
    def notify(self, args):
        global ui
        global selectedSketch
        
        try:                         
            selectedSketch = adsk.fusion.Sketch.cast(args.selection.entity)
        except:
            if ui:
                ui.messageBox('Failed:\n{}'.format(traceback.format_exc()))

# Unselected Handler                
class MyUnSelectHandler(adsk.core.SelectionEventHandler):
    def __init__(self):
        super().__init__()
    def notify(self, args):
        global ui

        try:
            selectedSketch = None
        except:
            if ui:
                ui.messageBox('Failed:\n{}'.format(traceback.format_exc()))         

# Event handler that reacts to when the command is destroyed. This terminates the script.            
# class onDestryHandler(adsk.core.CommandEventHandler):
#     def __init__(self):
#         super().__init__()
#     def notify(self, args):
#         global ui
#         try:
#             # When the command is done, terminate the script
#             # This will release all globals which will remove all event handlers
#             ui.messageBox('On Destroy')
#         except:
#             _ui.messageBox('Failed:\n{}'.format(traceback.format_exc()))                