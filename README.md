# README #

This Repository contains Autodesk Fusion 360 Addins.
Detailed information about this project can be found in the [Wiki](https://bitbucket.org/lu122131/adsk_addins/wiki/Home).

I add Addins when I need them or in case of requests.
If someone want to contribute it's always welcomed

### List Addins and last working release ###

* **Import Points from CSV:** From _ADSK_AddIns_Release_1.1.0_ - Download: [zip](https://bitbucket.org/lu122131/adsk_addins/get/ADSK_AddIns_Release_1.1.0.zip) [gz](https://bitbucket.org/lu122131/adsk_addins/get/ADSK_AddIns_Release_1.1.0.tar.gz) [bz2](https://bitbucket.org/lu122131/adsk_addins/get/ADSK_AddIns_Release_1.1.0.tar.bz2)


### How to collaborate ###

* [Fork the repository](https://bitbucket.org/lu122131/adsk_addins/fork)
* Work on it as much as you like
* Pull the branch with your work to the origin in your forked repository
* Create a pull-request on my remote _develop_ branch, i.e., go to _https:// bitbucket.org/<yourWorkspace\>/adsk_addins_forked/pull-requests/new_

### Who do I talk to? ###

* [Open an issue here](https://bitbucket.org/lu122131/adsk_addins/issues)

### Disclaimer ###
This software product is provided **"as is"** and **"with all faults"**. There are no representations or warranties of any kind concerning the safety, suitability, lack of viruses, inaccuracies, typographical errors, or other harmful components of this software product. There are inherent dangers in the use of any software, and you are solely responsible for determining whether this software product is compatible with your equipment and other software installed on your equipment. You are also solely responsible for the protection of your equipment and backup of your data, and the developer will not be liable for any damages you may suffer in connection with using, modifying, or distributing this software product.
